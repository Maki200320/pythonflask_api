from flask import Flask, jsonify, render_template, request, redirect, session, url_for
import pymysql

app = Flask(__name__)

# Database configuration
db = pymysql.connect(host="localhost", user="root", password="", database="cruduser")
cursor = db.cursor()
# ------------------------------------------------------------------------------------------------------------------------------

app.secret_key = 'maMaMo'

# Route to display the registration form
@app.route('/')
def registration_form():
    return render_template('register.html')

# Route for user registration
@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        try:
            username = request.form['username']
            email = request.form['email']
            password = request.form['password']

            # Check if the username already exists in the database
            check_username_query = "SELECT * FROM user WHERE username = %s"
            cursor.execute(check_username_query, (username,))
            existing_user = cursor.fetchone()

            if existing_user:
                registration_error = "Username already exists. Please choose a different username."
                return render_template('register.html', registration_error=registration_error)

            insert_user_query = "INSERT INTO user (username, email, password) VALUES (%s, %s, %s)"
            insert_user_values = (username, email, password)
            cursor.execute(insert_user_query, insert_user_values)

            db.commit()
            registration_success = True

            return redirect(url_for('login'))  # Redirect to the login route

        except pymysql.Error as e:
            return f"Error: {e}"

    if request.method == 'GET':
        return render_template('register.html')  # Display the registration form


# Route for user login
@app.route('/login', methods=['POST', 'GET'])
def login():
    login_error = None

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        connection = pymysql.connect(host="localhost", user="root", password="", database="cruduser")
        cursor = connection.cursor()

        # Query to check if the username and password match in the database
        query = "SELECT * FROM user WHERE username = %s AND password = %s"
        cursor.execute(query, (username, password))
        user = cursor.fetchone()

        if user:
            # Successful login, you can set a session variable to keep the user logged in
            session['user_id'] = user[0]  # Assuming 'id' is the user ID column name

            # Fetch all users from the database
            cursor.execute("SELECT * FROM user")
            users = cursor.fetchall()

            return render_template('index.html', users=users)

        else:
            login_error = "Username and password incorrect"  # Set login_error

    if request.method == 'GET':
        login_error = None

    return render_template('login.html', login_error=login_error)

# Routes for CRUD operations
@app.route('/index')
def index():
    # Fetch all users from the database
    cursor.execute("SELECT * FROM user")
    users = cursor.fetchall()
    return render_template('index.html', users=users)

# Route to add a new user
@app.route('/add', methods=['POST'])
def add_user():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']

        # Check if the username already exists in the database
        check_username_query = "SELECT * FROM user WHERE username = %s"
        cursor.execute(check_username_query, (username,))
        existing_user = cursor.fetchone()

        if existing_user:
            error_message = "Username is already in use. Please choose a different username."
            # Fetch all users from the database
            cursor.execute("SELECT * FROM user")
            users = cursor.fetchall()
            return render_template('index.html', users=users, error_message=error_message)
        else:
            # Username is available, proceed to insert the user into the database
            insert_user_query = "INSERT INTO user (username, password, email) VALUES (%s, %s, %s)"
            cursor.execute(insert_user_query, (username, password, email))
            db.commit()

    return redirect(url_for('index'))



# @app.route('/edit/<int:id>')
# def edit_user(id):
#     cursor.execute("SELECT * FROM user WHERE id = %s", (id,))
#     user = cursor.fetchone()
#     return render_template('edit.html', user=user)

@app.route('/update/<int:id>', methods=['POST'])
def update_user(id):
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']

        # Update user in the database
        cursor.execute("UPDATE user SET username=%s, password=%s, email=%s WHERE id=%s",
                       (username, password, email, id))
        db.commit()

    return redirect(url_for('index'))

@app.route('/delete/<int:id>')
def delete_user(id):
    # Delete user from the database
    cursor.execute("DELETE FROM user WHERE id = %s", (id,))
    db.commit()
    return redirect(url_for('index'))

@app.route('/get_user/<int:id>')
def get_user(id):
    cursor.execute("SELECT id, username, password, email FROM user WHERE id = %s", (id,))
    user = cursor.fetchone()
    if user:
        user_data = {
            'id': user[0],
            'username': user[1],
            'password': user[2],
            'email': user[3]
        }
        return jsonify(user_data)
    else:
        return jsonify({'error': 'User not found'})
    
@app.route('/logout')
def logout():
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(debug=True)
